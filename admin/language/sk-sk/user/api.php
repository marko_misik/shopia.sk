<?php
// Heading
$_['heading_title']        = 'API (Programovacie rozhranie aplikácie)';

// Text
$_['text_success']         = 'Programovacie rozhranie bolo úspešne upravené!';
$_['text_list']            = 'Zoznam aplikácií';
$_['text_add']             = 'Pridať aplikáciu';
$_['text_edit']            = 'Upraviť aplikáciu';
$_['text_ip']              = 'Nižšie si môžete vytvoriť zoznam IP adries povolený pre prístup k API. Vaša súčasná IP adresa je %s';

// Column
$_['column_name']          = 'API názov';
$_['column_status']        = 'Stav';
$_['column_date_added']    = 'Dátum pridania';
$_['column_date_modified'] = 'Dátum úpravy';
$_['column_token']         = 'Token';
$_['column_ip']            = 'IP adresa';
$_['column_action']        = 'Akcia';

// Entry
$_['entry_name']           = 'API názov';
$_['entry_key']            = 'API kľúč';
$_['entry_status']         = 'Stv';
$_['entry_ip']             = 'IP adresa';

// Error
$_['error_permission']     = 'Upozornenie: Nemáte oprávnenie k zmenám aplikácií!';
$_['error_name']           = 'API názov musí byť dlhší ako 3 znaky a kratší než 20 znakov!';
$_['error_key']            = 'API kľúč musí byť dlhší ako 64 znakov a kratší než 256 znakov!';