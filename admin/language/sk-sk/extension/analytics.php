<?php
// Heading
$_['heading_title']    = 'Analýzi';

// Text
$_['text_success']     = 'Analýzi boli úspešne upravené!';
$_['text_list']        = 'Zoznam analýz';

// Column
$_['column_name']      = 'Názov analýzi';
$_['column_status']    = 'Stav';
$_['column_action']    = 'Akcia';

// Error
$_['error_permission'] = 'Upozornenie: Nemáte oprávnenie pre správu analýz!';