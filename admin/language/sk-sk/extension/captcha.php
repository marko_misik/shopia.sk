<?php
// Heading
$_['heading_title']    = 'Kód overenia';

// Text
$_['text_success']     = 'Kód overenia bol uspešne upravený!';
$_['text_list']        = 'Zoznam kódov';

// Column
$_['column_name']      = 'Názov kódu';
$_['column_status']    = 'Stav';
$_['column_action']    = 'Akcia';

// Error
$_['error_permission'] = 'Upozornenie: Nemáte oprávnenie pre správu kódov overenia!';