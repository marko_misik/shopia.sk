<?php
// Heading                    
$_['heading_title']                    = 'Obchody';

// Text
$_['text_settings']                    = 'Nastavenia';
$_['text_success']                     = 'Obchody boli úspešne upravené!';
$_['text_list']                        = 'Zoznam obchodov';
$_['text_add']                         = 'Pridať obchode';
$_['text_edit']                        = 'Upraviť obchod';
$_['text_items']                       = 'Položky';
$_['text_tax']                         = 'Dane';
$_['text_account']                     = 'Účet';
$_['text_checkout']                    = 'Pokladňa';
$_['text_stock']                       = 'Sklad';
$_['text_shipping']                    = 'Adresa pre dopravu';
$_['text_payment']                     = 'Adresa pre platbu';

// Column
$_['column_name']                      = 'Meno obchodu';
$_['column_url']	                     = 'URL obchodu';
$_['column_action']                    = 'Akcia';

// Entry
$_['entry_url']                        = 'URL obchodu';
$_['entry_ssl']                        = 'Použiť SSL';
$_['entry_meta_title']                 = 'Titulka (meta-tag)';
$_['entry_meta_description']           = 'Popis (meta-tag)';
$_['entry_meta_keyword']               = 'Kľúčové slová (meta-tag)';
$_['entry_layout']                     = 'Predvolená šablóna';
$_['entry_theme']                      = 'Šablóna';
$_['entry_name']                       = 'Meno obchodu:';
$_['entry_owner']                      = 'Majiteľ obchodu:';
$_['entry_address']                    = 'Adresa';
$_['entry_geocode']                    = 'Geo kód';
$_['entry_email']                      = 'E-mail';
$_['entry_telephone']                  = 'Telefón';
$_['entry_fax']                        = 'Fax';
$_['entry_image']                      = 'Obrázok';
$_['entry_open']                       = 'Otváracie hodiny';
$_['entry_comment']                    = 'Komentár';
$_['entry_location']                   = 'Poloha obchodu';
$_['entry_country']                    = 'Štát';
$_['entry_zone']                       = 'Kraj';
$_['entry_language']                   = 'Jazyk';
$_['entry_currency']                   = 'Mena';
$_['entry_tax']                        = 'Zobraziť ceny s daňou';
$_['entry_tax_default']                = 'Použitie adresy pre výpočet dane';
$_['entry_tax_customer']               = 'Použitie predvolenej adresy';
$_['entry_customer_group']             = 'Zákaznícka skupina';
$_['entry_customer_group_display']     = 'Zákaznícke skupiny';
$_['entry_customer_price']             = 'Zobrazenie cien';
$_['entry_account']                    = 'Podmienky účtu';
$_['entry_cart_weight']                = 'Zobrazenie váhy v košíku';
$_['entry_checkout_guest']             = 'Nákup bez registrácie';
$_['entry_checkout']                   = 'Podmienky pokladne';
$_['entry_order_status']               = 'Stav objednávky';
$_['entry_stock_display']              = 'Zobrazovať stav skladu';
$_['entry_stock_checkout']             = 'Kontrola skladu';
$_['entry_logo']                       = 'Logo obchodu';
$_['entry_icon']                       = 'Ikona obchodu';
$_['entry_secure']                     = 'Použiť SSL certifikát';

// Help
$_['help_url']                         = 'Vložte URL adresu obchodu. Musí byť s \'/\' na konci. Príklad: http://wwww.vasadomena.sk/cesta/';
$_['help_ssl']                         = 'Pred zapnutím SSL zkontrolujte či máte nainštalovaný SSL certifikát';
$_['help_geocode']                     = 'Prosím, zadajte svoj geo kód ručne.';
$_['help_open']                        = 'Vyplňte v obchode otváracie doby.';
$_['help_comment']                     = 'Toto pole slúži na zvláštne poznámky, ktoré chcete oznámiť zákazníkovi. Napr.: Obchod neprijíma šeky.';
$_['help_location']                    = 'Zadajte polohu vašho obchodu, aby ste ho zobrazili v kontaktnom folmulári.';
$_['help_currency']                    = 'Zmena predvolenej meny. Vymažte vyrovnávaciu pamäť prehliadača, aby ste videli zmeny a obnovte existujúce cookie.';
$_['help_tax_default']                 = 'Použitie adresy pre výpočet dane, ak nikto nie je prihlásený. Môžete použiť uloženú adresu pre zákazníkov, dopravu alebo platobnú adresu.';
$_['help_tax_customer']                = 'Použitie predvolenej adresy zákazníka, keď sa prihlási. Môžete sa rozhodnúť použiť predvolenú adresu pre zákazníkov, dopravu alebo platobnú adresu.';
$_['help_customer_group']              = 'Predvolená skupina zákazníkov.';
$_['help_customer_group_display']      = 'Zobrazí zákaznícke skupiny.';
$_['help_customer_price']              = 'Zobrazovať ceny len po prihlásení zákazníka.';
$_['help_account']                     = 'Vynútiť odsúhlasenie podmienok pre vytvorenie účtu.';
$_['help_checkout_guest']              = 'Povoliť zákazníkom nákup bez registrácie v obchode.';
$_['help_checkout']                    = 'Vynútiť odsúhlasenie podmienok v pokladni.';
$_['help_order_status']                = 'Nastavte východiskový stav objednávky, ak je spracovaná nová objednávka.';
$_['help_stock_display']               = 'Zobrazovať množstvo tovaru na stránke produktu.';
$_['help_stock_checkout']              = 'Povoliť zákazníkom zadanie objednávky, keď tovar nieje skladom.';
$_['help_icon']                        = 'Ikona by mala byť vo formáte PNG s rozmermi 16px x 16px.';
$_['help_secure']                      = 'Pre používanie SSL certifikátu potrebujete server podporujúci SSL a nastavenie SSL v konfiguračnom súbore obchodu.';

// Error
$_['error_warning']                    = 'Upozornenie: Skontrolujte pole s chybami!';
$_['error_permission']                 = 'Upozornenie: Nemáte oprávnenie pre správu nastavenia obchodu!';
$_['error_url']                        = 'URL adresa obchodu je povinný údaj!';
$_['error_meta_title']                 = 'Titulka musí byť dlhšia ako 3 znaky a kratšia než 32 znakov!';
$_['error_name']                       = 'Názov obchodu musí byť dlhší ako 3 znaky a kratší než 32 znakov!';
$_['error_owner']                      = 'Majiteľ obchodu musí byť dlhší ako 3 znaky a kratší než 64 znakov!';
$_['error_address']                    = 'Adresa obchodu musí byť dlhšia ako 10 znakov a kratšia než 256 znakov';
$_['error_email']                      = 'E-mailová adresa je neplatná!';
$_['error_telephone']                  = 'Telefónne číslo musí byť dlhšie ako 3 znaky a kratšie než 32 znakov!';
$_['error_customer_group_display']     = 'Musí byť nastavená predvolená skupina zákazníkov, ak sa chystáte použiť túto funkciu!!';
$_['error_default']                    = 'Upozornenie: Nemôžete zmazať svoj predvolený obchod!';
$_['error_store']                      = 'Upozornenie: Tento obchod nemôže byť vymazaný, pretože je v súčasnosti priradený k %s objednávke(ám)!';