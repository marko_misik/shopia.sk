<?php
$_['heading_title']    = 'Základný kód overenia';

// Text
$_['text_captcha']     = 'Kód overenia';
$_['text_success']	   = 'Základný kód overenia bol úspešne upravený!';
$_['text_edit']        = 'Upraviť základný kód overenia';

// Entry
$_['entry_status']     = 'Stav';

// Error
$_['error_permission'] = 'Upozornenie: Nemáte oprávnenie pre spravu základeho kódu overenia!';