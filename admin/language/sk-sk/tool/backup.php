<?php
// Heading
$_['heading_title']    = 'Záloha/Obnova';

// Text
$_['text_success']     = 'Databáza bola úspešne importována!';

// Entry
$_['entry_import']     = 'Import';
$_['entry_export']     = 'Export';

// Error
$_['error_permission'] = 'Upozornenie: Nemáte oprávnenie pre správu záloh!';
$_['error_export']     = 'Upozornenie: Musíte vybrať aspoň jednu tabuľku pre export!';
$_['error_empty']      = 'Upozornenie: Nahrávaný súbor je prázdny!';