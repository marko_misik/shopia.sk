<?php
// HTTP
define('HTTP_SERVER', 'http://shopia.demo.wequire.sk/');

// HTTPS
define('HTTPS_SERVER', 'http://shopia.demo.wequire.sk/');

// DIR
define('DIR_APPLICATION', '/www/shopia.sk/www/catalog/');
define('DIR_SYSTEM', '/www/shopia.sk/www/system/');
define('DIR_IMAGE', '/www/shopia.sk/www/image/');
define('DIR_LANGUAGE', '/www/shopia.sk/www/catalog/language/');
define('DIR_TEMPLATE', '/www/shopia.sk/www/catalog/view/theme/');
define('DIR_CONFIG', '/www/shopia.sk/www/system/config/');
define('DIR_CACHE', '/www/shopia.sk/www/system/storage/cache/');
define('DIR_DOWNLOAD', '/www/shopia.sk/www/system/storage/download/');
define('DIR_LOGS', '/www/shopia.sk/www/system/storage/logs/');
define('DIR_MODIFICATION', '/www/shopia.sk/www/system/storage/modification/');
define('DIR_UPLOAD', '/www/shopia.sk/www/system/storage/upload/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'shopia.sk');
define('DB_PASSWORD', 'h2khg*fns');
define('DB_DATABASE', 'shopia_sk_0');
define('DB_PORT', '3306');
define('DB_PREFIX', 'shopia2016_');
