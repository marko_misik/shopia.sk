<?php
// Text
$_['text_success'] = 'API (Programovacie rozhranie aplikácie) bolo úspešne spustené!';

// Error
$_['error_key']    = 'Upozornenie: Neplatný API kľúč!';
$_['error_ip']     = 'Upozornenie: IP adresa %s nemá povolenie pre prístup k tejto API (Programovacie rozhranie aplikácie)!';