<?php
// Text
$_['text_captcha']  = 'Základný kód overenia';

// Entry
$_['entry_captcha'] = 'Zadajte kód do poľa nižšie';

// Error
$_['error_captcha'] = 'Overovací kód nezodpovedá obrázku!';