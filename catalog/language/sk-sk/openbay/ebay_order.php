<?php
// Text
$_['text_total_shipping']	= 'Doprava';
$_['text_total_discount']	= 'Zľava';
$_['text_total_tax']			= 'Daň';
$_['text_total_sub']			= 'Medzi-súčet';
$_['text_total']				  = 'Celkom';
$_['text_smp_id']			  	= 'ID správcu predaja: ';
$_['text_buyer']				  = 'Meno kupujucého: ';