<!-- banner -->
<div class="banner-grid">
  <div id="visual-<?php echo $module; ?>">
      <div class="slide-visual">
        <!-- Slide Image Area (1000 x 424) -->
        <ul class="slide-group">
          <?php foreach ($banners as $banner) { ?>
            <li>
            <?php if ($banner['link']) { ?>
              <a href="<?php echo $banner['link']; ?>">
                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                </a>
            <?php } else { ?>
              <img class="img-responsive" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
            <?php } ?>
            </li>
          <?php } ?>
        </ul>

        <!-- Slide Description Image Area (316 x 328) -->
        <? /*<div class="script-wrap">
          <ul class="script-group">
            <?php foreach ($banners as $banner) { ?>
              <li>
                <div class="inner-script">
                  <img class="img-responsive" src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?> thumb" />
                </div>
              </li>
            <?php } ?>
          </ul>
          <div class="slide-controller">
            <a href="#" class="btn-prev"><img src="/design/images/btn_prev.png" alt="Prev Slide" /></a>
            <a href="#" class="btn-play"><img src="/design/images/btn_play.png" alt="Start Slide" /></a>
            <a href="#" class="btn-pause"><img src="/design/images/btn_pause.png" alt="Pause Slide" /></a>
            <a href="#" class="btn-next"><img src="/design/images/btn_next.png" alt="Next Slide" /></a>
          </div>
        </div>
        <div class="clearfix"></div>*/ ?>
      </div>
      <div class="clearfix"></div>
    </div>
    <script type="text/javascript">
    //<![CDATA[
      $(window).load(function() {
        $('#visual-<?php echo $module; ?>').pignoseLayerSlider({
          // play    : '.btn-play',
          // pause   : '.btn-pause',
          next    : '.btn-next',
          prev    : '.btn-prev'
        });
      });
    //]]>
    </script>

</div>
<!-- //banner -->

<? return; ?>

<div id="slideshow<?php echo $module; ?>" class="owl-carousel" style="opacity: 1;">
  <?php foreach ($banners as $banner) { ?>
  <div class="item">
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
    <?php } ?>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
$('#slideshow<?php echo $module; ?>').owlCarousel({
	items: 6,
	autoPlay: 3000,
	singleItem: true,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: true
});
--></script>