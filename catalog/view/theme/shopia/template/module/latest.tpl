<div class="product-easy">
  <div class="container">
    <div class="sap_tabs">

      <div class="new_arrivals" style="padding-top: 0px;">
        <div class="container">
          <h3><?php echo $heading_title; ?></h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium</p>
        </div>
      </div>

      <?php foreach ($products as $product) { ?>

        <div class="col-md-3 product-men yes-marg">
          <div class="men-pro-item simpleCart_shelfItem">
            <div class="men-thumb-item">

              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="pro-image-front">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="pro-image-back">

              <div class="men-cart-pro">
                <div class="inner-men-cart-pro">
                  <a href="<?php echo $product['href']; ?>" class="link-product-add-cart">Rýchly náhľad</a>
                </div>
              </div>
              <span class="product-new-top">Novinka</span>
            </div>
            <div class="item-info-product">
              <h4>
                <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              </h4>

              <?php if ($product['price']) { ?>
                <div class="info-product-price">

                  <?php if (!$product['special']) { ?>
                    <span class="item_price"><?php echo $product['price']; ?>

                      <?php if ($product['tax']) { ?>
                        <br /><small><small><?php echo $text_tax; ?> <?php echo $product['tax']; ?></small></small>
                      <?php } ?>

                    </span>
                  <?php } else { ?>
                    <span class="item_price"><?php echo $product['special']; ?>

                      <?php if ($product['tax']) { ?>
                        <br /><small><?php echo $text_tax; ?> <?php echo $product['tax']; ?></small>
                      <?php } ?>

                    </span>
                    <del><?php echo $product['price']; ?></del>
                  <?php } ?>
                </div>
              <?php } ?>

              <a href="<?php echo $product['href']; ?>" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="item_add single-item hvr-outline-out button2">Pridať do košíka</a>
            </div>
          </div>
        </div>

      <?php } ?>

    </div>
  </div>
</div>
