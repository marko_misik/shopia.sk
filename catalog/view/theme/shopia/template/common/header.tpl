<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="canonical shortcut icon" href="/design/images/favicon.ico" />

  <title><?php echo $title; ?></title>

  <base href="<?php echo $base; ?>" />
  <meta name="description" content="<?php echo $description; ?>" />
  <meta name="keywords" content= "<?php echo $keywords; ?>" />

  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
  <link href="/design/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/design/css/pignose.layerslider.css" rel="stylesheet" type="text/css" media="all" />
  <link href="/design/css/style.css" rel="stylesheet" type="text/css" media="all" />

  <script type="text/javascript" src="/design/js/jquery-2.1.4.min.js"></script>
  <script src="/design/js/simpleCart.min.js"></script>
  <script type="text/javascript" src="/design/js/bootstrap-3.1.1.min.js"></script>

  <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
  <script src="/design/js/jquery.easing.min.js"></script>

  <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">

  <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>

  <script src="catalog/view/javascript/common.js" type="text/javascript"></script>

  <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
  <?php } ?>

  <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
  <?php } ?>

  <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
  <?php } ?>

</head>
<body class="<?php echo $class; ?>">

<!-- header -->
<div class="header">
  <div class="container">
    <ul>
      <li><span class="glyphicon glyphicon-time" aria-hidden="true"></span>Rýchle dodanie</li>
      <li><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Pri objednávke do 14:00 dodanie v ďalší deň.</li>
      <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@shopia.sk">info@shopia.sk</a></li>
    </ul>
  </div>
</div>
<!-- //header -->

<!-- header-bot -->
<div class="header-bot">
  <div class="container">
    <div class="col-md-3 header-left">
      <h1>
        <a href="/">
          <img src="/design/images/logo.png" alt="Shopia.sk">
        </a>
      </h1>
    </div>
    <div class="col-md-6 header-middle">
      <?= $search; ?>
    </div>
    <div class="col-md-3 header-right footer-bottom">
      <ul>
        <li><a href="<?= $login; ?>" class="use1" data-toggle="modal" data-target="#myModal4"><span>Login</span></a>
        </li>
        <li><a class="fb" href="#"></a></li>
        <li><a class="twi" href="#"></a></li>
        <li><a class="insta" href="#"></a></li>
        <li><a class="you" href="#"></a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- //header-bot -->

<!-- banner -->
<div class="ban-top">
  <div class="container">
    <div class="top_nav_left">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse menu--shylock" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav menu__list">
          <li class="active menu__item menu__item--current"><a class="menu__link" href="/">Home <span class="sr-only">(current)</span></a></li>
          <li class="dropdown menu__item">
            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">men's wear <span class="caret"></span></a>
              <ul class="dropdown-menu multi-column columns-3">
                <div class="row">
                  <div class="col-sm-6 multi-gd-img1 multi-gd-text ">
                    <a href="mens.html"><img src="images/woo1.jpg" alt=" "/></a>
                  </div>
                  <div class="col-sm-3 multi-gd-img">
                    <ul class="multi-column-dropdown">
                      <li><a href="mens.html">Clothing</a></li>
                      <li><a href="mens.html">Wallets</a></li>
                      <li><a href="mens.html">Footwear</a></li>
                      <li><a href="mens.html">Watches</a></li>
                      <li><a href="mens.html">Accessories</a></li>
                      <li><a href="mens.html">Bags</a></li>
                      <li><a href="mens.html">Caps & Hats</a></li>
                    </ul>
                  </div>
                  <div class="col-sm-3 multi-gd-img">
                    <ul class="multi-column-dropdown">
                      <li><a href="mens.html">Jewellery</a></li>
                      <li><a href="mens.html">Sunglasses</a></li>
                      <li><a href="mens.html">Perfumes</a></li>
                      <li><a href="mens.html">Beauty</a></li>
                      <li><a href="mens.html">Shirts</a></li>
                      <li><a href="mens.html">Sunglasses</a></li>
                      <li><a href="mens.html">Swimwear</a></li>
                    </ul>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </ul>
          </li>
          <li class="dropdown menu__item">
            <a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">women's wear <span class="caret"></span></a>
              <ul class="dropdown-menu multi-column columns-3">
                <div class="row">
                  <div class="col-sm-3 multi-gd-img">
                    <ul class="multi-column-dropdown">
                      <li><a href="womens.html">Clothing</a></li>
                      <li><a href="womens.html">Wallets</a></li>
                      <li><a href="womens.html">Footwear</a></li>
                      <li><a href="womens.html">Watches</a></li>
                      <li><a href="womens.html">Accessories</a></li>
                      <li><a href="womens.html">Bags</a></li>
                      <li><a href="womens.html">Caps & Hats</a></li>
                    </ul>
                  </div>
                  <div class="col-sm-3 multi-gd-img">
                    <ul class="multi-column-dropdown">
                      <li><a href="womens.html">Jewellery</a></li>
                      <li><a href="womens.html">Sunglasses</a></li>
                      <li><a href="womens.html">Perfumes</a></li>
                      <li><a href="womens.html">Beauty</a></li>
                      <li><a href="womens.html">Shirts</a></li>
                      <li><a href="womens.html">Sunglasses</a></li>
                      <li><a href="womens.html">Swimwear</a></li>
                    </ul>
                  </div>
                  <div class="col-sm-6 multi-gd-img multi-gd-text ">
                    <a href="womens.html"><img src="images/woo.jpg" alt=" "/></a>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </ul>
          </li>
          <li class=" menu__item"><a class="menu__link" href="electronics.html">Electronics</a></li>
          <li class=" menu__item"><a class="menu__link" href="codes.html">Short Codes</a></li>
          <li class=" menu__item"><a class="menu__link" href="contact.html">contact</a></li>
          </ul>
        </div>
        </div>
      </nav>
    </div>
    <div class="top_nav_right">
      <?= $cart; ?>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<!-- //banner-top -->


<? return; ?>

<nav id="top">
  <div class="container">
    <?php echo $currency; ?>
    <?php echo $language; ?>
    <div id="top-links" class="nav pull-right">
      <ul class="list-inline">
        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>
        <li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul class="dropdown-menu dropdown-menu-right">
            <?php if ($logged) { ?>
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
            <?php } else { ?>
            <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
            <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
            <?php } ?>
          </ul>
        </li>
        <li><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a></li>
        <li><a href="<?php echo $checkout; ?>" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_checkout; ?></span></a></li>
      </ul>
    </div>
  </div>
</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
          <?php if ($logo) { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>
