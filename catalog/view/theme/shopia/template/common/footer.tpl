<div class="coupons">
  <div class="container">
    <div class="coupons-grids text-center">
      <div class="col-md-3 coupons-gd">
        <h3>Buy your product in a simple way</h3>
      </div>
      <div class="col-md-3 coupons-gd">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
        <h4>LOGIN TO YOUR ACCOUNT</h4>
        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor
      sit amet, consectetur.</p>
      </div>
      <div class="col-md-3 coupons-gd">
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
        <h4>SELECT YOUR ITEM</h4>
        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor
      sit amet, consectetur.</p>
      </div>
      <div class="col-md-3 coupons-gd">
        <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
        <h4>MAKE PAYMENT</h4>
        <p>Neque porro quisquam est, qui dolorem ipsum quia dolor
      sit amet, consectetur.</p>
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
</div>

<!-- footer -->
<div class="footer">
  <div class="container">
    <div class="col-md-3 footer-left">
      <h2>
        <a href="/">
          <img src="/design/images/logo.png" alt="Shopia" />
        </a>
      </h2>
      <p>Neque porro quisquam est, qui dolorem ipsum quia dolor
      sit amet, consectetur, adipisci velit, sed quia non 
      numquam eius modi tempora incidunt ut labore 
      et dolore magnam aliquam quaerat voluptatem.</p>
    </div>
    <div class="col-md-9 footer-right">
      <div class="col-sm-6 newsleft">
        <h3>ODBER NOVINIEK !</h3>
      </div>
      <div class="col-sm-6 newsright">
        <form>
          <input type="text" value="E-mail" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
          <input type="submit" value="ODOSLAŤ">
        </form>
      </div>
      <div class="clearfix"></div>
      <div class="sign-grds">
        <div class="col-md-4 sign-gd">
          <h4>Informácie</h4>
          <ul>
            <li><a href="/">Domov</a></li>
            <li><a href="mens.html">Men's Wear</a></li>
            <li><a href="womens.html">Women's Wear</a></li>
            <li><a href="electronics.html">Electronics</a></li>
            <li><a href="codes.html">Short Codes</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-4 sign-gd-two">
          <h4>Kontaktujte nás</h4>
          <ul>
            <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Nová 406, <span>045 01 Mokrance</span></li>
            <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@shopia.sk">info@shopia.sk</a></li>
            <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Telefón : <a href="tel:+421940819945">+421 940 819 945</a></li>
          </ul>
        </div>
        <div class="col-md-4 sign-gd flickr-post">
          <h4>Posts</h4>
          <ul>
            <li><a href="single.html"><img src="/design/images/b15.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b16.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b17.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b18.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b15.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b16.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b17.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b18.jpg" alt=" " class="img-responsive" /></a></li>
            <li><a href="single.html"><img src="/design/images/b15.jpg" alt=" " class="img-responsive" /></a></li>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="clearfix"></div>
    <p class="copy-right">&copy; 2016 Shopia. All rights reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> | Created by <a href="http://www.wequire.sk" target="_blank" title="Tvorba webstránok">WeQuire.sk</a></p>
  </div>
</div>
<!-- //footer -->

<!-- login -->
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-info">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>            
      </div>
      <div class="modal-body modal-spa">
        <div class="login-grids">
          <div class="login">
            <div class="login-bottom">
              <h3>Sign up for free</h3>
              <form>
                <div class="sign-up">
                  <h4>Email :</h4>
                  <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required=""> 
                </div>
                <div class="sign-up">
                  <h4>Password :</h4>
                  <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                  
                </div>
                <div class="sign-up">
                  <h4>Re-type Password :</h4>
                  <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                  
                </div>
                <div class="sign-up">
                  <input type="submit" value="REGISTER NOW" >
                </div>
                
              </form>
            </div>
            <div class="login-right">
              <h3>Sign in with your account</h3>
              <form>
                <div class="sign-in">
                  <h4>Email :</h4>
                  <input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required=""> 
                </div>
                <div class="sign-in">
                  <h4>Password :</h4>
                  <input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                  <a href="#">Forgot password?</a>
                </div>
                <div class="single-bottom">
                  <input type="checkbox"  id="brand" value="">
                  <label for="brand"><span></span>Remember Me.</label>
                </div>
                <div class="sign-in">
                  <input type="submit" value="SIGNIN" >
                </div>
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- //login -->
</body>
</html>

<? return; ?>

<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>

</body></html>